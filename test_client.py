#!/usr/bin/env python3
import requests
import json
import base64
import numpy as np
import cv2
import sys
sys.path.append('/home/devo/motion-project-interface/')
from visualizers import visualize_faces

base_url = "http://0.0.0.0:5000/model/api/v1.0/"

headers = {
  'Content-Type': 'application/json'
}

if __name__ == '__main__':
  cap = cv2.VideoCapture('/home/devo/Downloads/Drinking.mp4')
  while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    requests.request("PUT", base_url+'test_connection')
    image = visualize_faces(frame)
    requests.request("PUT", base_url+'test_connection')
    # Display the resulting frame
    cv2.imshow('frame', image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # When everything done, release the capture
  cap.release()
  cv2.destroyAllWindows()

