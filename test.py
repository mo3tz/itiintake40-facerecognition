import base64
import json
from collections import Counter
from flask import Flask, request, redirect, jsonify, url_for, abort, make_response


app = Flask(__name__)

@app.route('/model/api/v1.0/recognize', methods=['GET'])
def reconize_image():
    return make_response(jsonify({'Status: ': 'finished'}), 200)

if __name__ == '__main__':
    app.run(host='0.0.0.0')