# **Human Face Identification**
By *Moetaz Mohamed Mokhtar*

***Still under construction***

## License
The source code in this repo and all the sub repos inside it falls under MIT License with no restriction or limitation in any research or acadmic usages.

the pretrained models in this repo is available only for non-commercial research purposes only.

## Introduction
In this repo, we provide source codes to achieve deep face recognition to datasets with low number of images per class using KNN classifiers to compare images with the dataset.

## Getting started
These instructions will get you a running copy of the project up for development and testing purposes.

All the comands assumes you are in **the same folder as the file you are trying to run** so make changes to the path depending on your file system and current working directory

**Using Google Colab**

You can find the `.ipynb` file in the repo if you followed this path
```
https://gitlab.com/mo3tz/itiintake40-facerecognition/-/blob/master/Face_Recognition.ipynb
```
you can download it and then use your google drive to open the file in google colab app

*Don't forget to set the Hardware acceleration to **GPU***

You will find extra features in the colab file that doesn't exist in the repo file like data augmentation.

**Using Docker**

You can find the `Docker` file in the repo if you followed this path
```
https://gitlab.com/mo3tz/itiintake40-facerecognition/-/blob/master/Dockerfile
```
the docker image is ubuntu based.
after downloading it to your PC run the following command
```
$ Docker build Dockerfile
```
to call the code APIs use
```
# to be added after API is done
```

**Using your personal environment**

clone the repo to your PC
```
$ git clone --recursive https://gitlab.com/mo3tz/itiintake40-facerecognition.git
```
Download required libraries
```
$ cd ./itiintake40-face_recognition/
$ pip3 install -r requirements.txt
```
run the main source code by
```
$ python Face_recognition_main.py ${input_image path} ${output directory} ${face detection model} ${face recognition model} ${Path to KNN features & labels}
```
Base case:
```
${face detection model} --> './itiintake40_facerecognition/insightface/models/R50,0000' 
${face recognition model} --> './itiintake40_facerecognition/facenet/model/20180402-114759/'
${Path to KNN features & labels} --> './itiintake40_facerecognition/data/'
```
the output should be an array with the recognized faces and the bounding box to each face

## Training
In the training all images are aligned using MTCNN and then resized to 160x160 then a 512 feature vector and the image label for each images is saved in a numpy binary files.

```
# Added after implementing and new face and its API 
```

## Pretrained Models
Detection models

we used LResNet50E in project construction `./itiintake40_facerecognition/insightface/models/`


| Method           | LFW(%)   | CFP-FP(%) | AgeDB-30(%) | MegaFace(%) | Download  |  
| -------          | ------   | --------- | ----------- |-------------|-----------| 
| LResNet100E      | 99.77    | 98.27     | 98.28       | 98.47       | [baidu cloud](https://pan.baidu.com/s/1wuRTf2YIsKt76TxFufsRNA) and [dropbox](https://www.dropbox.com/s/tj96fsm6t6rq8ye/model-r100-arcface-ms1m-refine-v2.zip?dl=0) |
| LResNet50E      | 99.80     | 92.74     | 97.76       | 97.64       | [baidu cloud](https://pan.baidu.com/s/1mj6X7MK) and [dropbox](https://www.dropbox.com/s/ou8v3c307vyzawc/model-r50-arcface-ms1m-refine-v1.zip?dl=0) |
| LResNet34E      | 99.65     | 92.12     | 97.70       | 96.70       | [baidu cloud](https://pan.baidu.com/s/1jKahEXw) and [dropbox](https://www.dropbox.com/s/yp7y6jic464l09z/model-r34-arcface-ms1m-refine-v1.zip?dl=0) |
| MobileFaceNet      | 99.50     | 88.94     | 95.91       |------- | [baidu cloud](https://pan.baidu.com/s/1If28BkHde4fiuweJrbicVA) and [dropbox](https://www.dropbox.com/s/akxeqp99jvsd6z7/model-MobileFaceNet-arcface-ms1m-refine-v1.zip?dl=0) |

Recognition models

we used VGGFace2 model in project construction `./itiintake40_facerecognition/facenet/model/20180402-114759/`


| Model name      | LFW accuracy | Training dataset | Architecture |
|-----------------|--------------|------------------|-------------|
| [20180408-102900](https://drive.google.com/open?id=1R77HmFADxe87GmoLwzfgMu_HY0IhcyBz) | 0.9905        | CASIA-WebFace    | [Inception ResNet v1](https://github.com/davidsandberg/facenet/blob/master/src/models/inception_resnet_v1.py) |
| [20180402-114759](https://drive.google.com/open?id=1EXPBSXwTaqrSC0OhUdXNmKSh9qJUQ55-) | 0.9965        | VGGFace2      | [Inception ResNet v1](https://github.com/davidsandberg/facenet/blob/master/src/models/inception_resnet_v1.py) |

## Face Alignment
please check [Facenet](https://github.com/davidsandberg/facenet) repository for more info

## Face Detection
please check [insightface/RetinaFace](https://github.com/deepinsight/insightface/tree/master/RetinaFace) for more info

# Contact

[Moetaz Mohamed Mokhtar](moetazmohamed96@gmail.com)
