# For more information, please refer to https://aka.ms/vscode-docker-python
FROM ubuntu
# FROM nvidia/cuda:10.2-cudnn7-devel

#application port
EXPOSE 5000

#Download pacjages
RUN apt update -- && apt install -y \
    python \
    git \
    python3-pip \
    python3-setuptools \
    libsm6 \
    libxext6 \
    libxrender1

# Disable Running performance tests to find the best convolution algorithm for face detection
ENV MXNET_CUDNN_AUTOTUNE_DEFAULT 0

# Install pip requirements
ADD requirements.txt .
RUN pip3 install -r requirements.txt

COPY . /root/

WORKDIR /root/
RUN cd insightface/RetinaFace/rcnn/cython ;  python3 setup.py build_ext --inplace
ENTRYPOINT /bin/bash
# ENTRYPOINT /bin/bash
